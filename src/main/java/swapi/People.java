package swapi;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class People {

    private String next;
    private List<Person> results;

    public People() {
    }

    /**
     * @return the next
     */
    public String getNext() {
        return next;
    }

    /**
     * @param next the next to set
     */
    public void setNext(String next) {
        this.next = next;
    }

    /**
     * @return the results
     */
    public List<Person> getResults() {
        return results;
    }

    /**
     * @param results the results to set
     */
    public void setResults(List<Person> results) {
        this.results = results;
    }

}