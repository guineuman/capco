package swapi;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Films {

    private String next;
    private List<Film> results;

    public Films() {
    }

    /**
     * @return the next
     */
    public String getNext() {
        return next;
    }

    /**
     * @param next the next to set
     */
    public void setNext(String next) {
        this.next = next;
    }

    /**
     * @return the results
     */
    public List<Film> getResults() {
        return results;
    }

    /**
     * @param results the results to set
     */
    public void setResults(List<Film> results) {
        this.results = results;
    }

}