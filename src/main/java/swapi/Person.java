package swapi;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Person {

    private String name;
    private String mass;
    private String birth_year;
    private List<String> films;
    private List<String> species;
    private String url;

    public Person() {
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the mass
     */
    public String getMass() {
        return mass;
    }

    /**
     * @param mass the mass to set
     */
    public void setMass(String mass) {
        this.mass = mass;
    }

    /**
     * @return the birth_year
     */
    public String getBirth_year() {
        return birth_year;
    }

    /**
     * @param birth_year the birth_year to set
     */
    public void setBirth_year(String birth_year) {
        this.birth_year = birth_year;
    }

    /**
     * @return the films
     */
    public List<String> getFilms() {
        return films;
    }

    /**
     * @param films the films to set
     */
    public void setFilms(List<String> films) {
        this.films = films;
    }

    /**
     * @return the species
     */
    public List<String> getSpecies() {
        return species;
    }

    /**
     * @param species the species to set
     */
    public void setSpecies(List<String> species) {
        this.species = species;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

}