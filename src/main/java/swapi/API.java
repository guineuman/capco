package swapi;

import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.*;

public class API {

    private String baseUrl;
    private static Pattern pattern = Pattern.compile("^\\D+(\\d+)/");

    public API(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public List<Person> getPeople() throws Exception {
        try {
            List<Person> results = new ArrayList<>();
            RestTemplate restTemplate = new RestTemplate();
            String url = baseUrl + "people/?format=json";
            while (url != null) {
                People people = restTemplate.getForObject(url, People.class);
                results.addAll(people.getResults());
                url = people.getNext();
            }
            return results;
        } catch (Exception e) {
            System.err.println("API.getPeople() error");
            throw e;
        }
    }

    public List<Film> getFilms() throws Exception {
        try {
            List<Film> results = new ArrayList<>();
            RestTemplate restTemplate = new RestTemplate();
            String url = baseUrl + "films/?format=json";
            while (url != null) {
                Films films = restTemplate.getForObject(url, Films.class);
                results.addAll(films.getResults());
                url = films.getNext();
            }
            return results;
        } catch (Exception e) {
            System.err.println("API.getFilms() error");
            throw e;
        }
    }

    public List<Specie> getSpecies() throws Exception {
        try {
            List<Specie> results = new ArrayList<>();
            RestTemplate restTemplate = new RestTemplate();
            String url = baseUrl + "species/?format=json";
            while (url != null) {
                Species species = restTemplate.getForObject(url, Species.class);
                results.addAll(species.getResults());
                url = species.getNext();
            }
            return results;
        } catch (Exception e) {
            System.err.println("API.getSpecies() error");
            throw e;
        }
    }

    public Long getIdFromUrl(String url) throws Exception {
        try {
            Long id = null;
            Matcher matcher = API.pattern.matcher(url);
            boolean find = matcher.find();
            String sid = matcher.group(1);
            if (find && sid != null) {
                id = Long.valueOf(sid);
            } else {
                throw new Exception("API.getIdFromUrl(" + url + ") find=" + find + " sid=" + sid);
            }
            return id;
        } catch (Exception e) {
            System.err.println("API.getIdFromUrl(" + url + ") error");
            throw e;
        }
    }

}