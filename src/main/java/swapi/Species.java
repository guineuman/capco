package swapi;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Species {

    private String next;
    private List<Specie> results;

    public Species() {
    }

    /**
     * @return the next
     */
    public String getNext() {
        return next;
    }

    /**
     * @param next the next to set
     */
    public void setNext(String next) {
        this.next = next;
    }

    /**
     * @return the results
     */
    public List<Specie> getResults() {
        return results;
    }

    /**
     * @param results the results to set
     */
    public void setResults(List<Specie> results) {
        this.results = results;
    }

}