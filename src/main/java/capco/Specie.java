package capco;

import java.util.*;
import javax.persistence.*;

@Entity
public class Specie {

    @Id
    @SequenceGenerator(name = "specie_generator", sequenceName = "specie_sequence", allocationSize = 1)
    @GeneratedValue(generator = "specie_generator")
    private long id;

    private String name;

    @ManyToMany(mappedBy = "species")
    private Set<Person> people = new HashSet<>();

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}
