package capco;

import java.util.*;
import javax.persistence.*;

@Entity
public class Film {

    @Id
    @SequenceGenerator(name = "film_generator", sequenceName = "film_sequence", allocationSize = 1)
    @GeneratedValue(generator = "film_generator")
    private long id;

    private String title;

    @ManyToMany(mappedBy = "films")
    private Set<Person> characters = new HashSet<>();

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
}
