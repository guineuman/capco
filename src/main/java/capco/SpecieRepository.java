package capco;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SpecieRepository extends JpaRepository<Specie, Long> {

    List<Specie> findByName(String name);

}
