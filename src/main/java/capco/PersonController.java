package capco;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
class PersonController {

    private final PersonRepository repository;
    private final SpecieRepository specieRepository;

    PersonController(PersonRepository repository, SpecieRepository specieRepository) {
        this.repository = repository;
        this.specieRepository = specieRepository;
    }

    @GetMapping("/people")
    List<Person> all() {
        
        return repository.findAllOrderByFilmsCountDescNameAsc();
    }

    @GetMapping("/people/{id}")
    Person one(@PathVariable Long id) {
        
        return repository.findById(id).orElseThrow(() -> new RuntimeException("Could not find person " + id));
    }

    @GetMapping("/people/humans")
    Map<String, Object> humans() {
        
        Map<String, Object> result = new HashMap<String, Object>();

        List<Person> humans = repository.findBySpecies(new HashSet<Specie>(specieRepository.findByName("Human")));
        float totalMass = 0;
        int knownMassCount = 0;
        
        for (Person human : humans) {
            try {
                float mass = Float.parseFloat(human.getMass());
                totalMass += mass;
                knownMassCount++;
            } catch (Exception e) {
                // mass is probably 'unknown', ignore it
            }
        }
        Float averageMass = Float.valueOf(totalMass / knownMassCount);

        result.put("humans", humans);
        result.put("averageMass", averageMass);
        return result;
    }

}