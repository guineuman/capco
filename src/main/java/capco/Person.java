package capco;

import java.util.*;
import javax.persistence.*;

@Entity
public class Person {

	@Id
	@SequenceGenerator(name = "person_generator", sequenceName = "person_sequence", allocationSize = 1)
	@GeneratedValue(generator = "person_generator")
	private long id;

	private String name;
	private String mass;
	private String birthYear;

	@ManyToMany
    @JoinTable(name="PERSON_FILMS", joinColumns=@JoinColumn(name="PERSON_ID"), inverseJoinColumns=@JoinColumn(name="FILM_ID"))
	private Set<Film> films = new HashSet<>();

	@ManyToMany
    @JoinTable(name="PERSON_SPECIES", joinColumns=@JoinColumn(name="PERSON_ID"), inverseJoinColumns=@JoinColumn(name="SPECIE_ID"))
	private Set<Specie> species = new HashSet<>();
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMass() {
		return mass;
	}

	public void setMass(String mass) {
		this.mass = mass;
	}

	public String getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}

	public Set<Film> getFilms() {
		return films;
	}

	public void setFilms(Set<Film> films) {
		this.films = films;
	}

	public Set<Specie> getSpecies() {
		return species;
	}

	public void setSpecies(Set<Specie> species) {
		this.species = species;
	}

}
