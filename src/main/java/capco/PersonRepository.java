package capco;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PersonRepository extends JpaRepository<Person, Long> {

    @Query(value = "select p, count(f) from Person p join p.films f group by p order by count(f) desc, p.name asc")
    List<Person> findAllOrderByFilmsCountDescNameAsc();

    List<Person> findBySpecies(Set<Specie> species);
}
