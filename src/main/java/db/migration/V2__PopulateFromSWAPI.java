package db.migration;

import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import java.sql.PreparedStatement;
import java.util.List;

import swapi.*;

public class V2__PopulateFromSWAPI extends BaseJavaMigration {

    public void migrate(Context context) throws Exception {
        try {
            API api = new API("https://swapi.co/api/");
            insertFilms(api, context);
            insertSpecies(api, context);
            insertPeople(api, context);
        } catch (Exception e) {
            System.err.println("V2__PopulateFromSWAPI.migrate() error");
            throw e;
        }
    }

    private void insertFilms(API api, Context context) throws Exception {
        try {
            List<Film> films = api.getFilms();
            long maxFilmId = 0;
            PreparedStatement statementInsertFilm = context.getConnection()
                    .prepareStatement("INSERT INTO film (id, title) VALUES (?, ?);");
            PreparedStatement statementInsertFilmSequence = context.getConnection()
                    .prepareStatement("INSERT INTO film_sequence (next_val) VALUES (?);");

            for (Film film : films) {
                long id = api.getIdFromUrl(film.getUrl()).longValue();

                statementInsertFilm.setLong(1, id);
                statementInsertFilm.setString(2, film.getTitle());
                statementInsertFilm.executeUpdate();

                if (id > maxFilmId) {
                    maxFilmId = id;
                }
            }
            statementInsertFilmSequence.setLong(1, maxFilmId + 1);
            statementInsertFilmSequence.executeUpdate();
        } catch (Exception e) {
            System.err.println("V2__PopulateFromSWAPI.insertFilms() error");
            throw e;
        }
    }

    private void insertSpecies(API api, Context context) throws Exception {
        try {
            List<Specie> species = api.getSpecies();
            long maxSpecieId = 0;
            PreparedStatement statementInsertSpecie = context.getConnection()
                    .prepareStatement("INSERT INTO specie (id, name) VALUES (?, ?);");
            PreparedStatement statementInsertSpecieSequence = context.getConnection()
                    .prepareStatement("INSERT INTO specie_sequence (next_val) VALUES (?);");

            for (Specie specie : species) {
                long id = api.getIdFromUrl(specie.getUrl()).longValue();

                statementInsertSpecie.setLong(1, id);
                statementInsertSpecie.setString(2, specie.getName());
                statementInsertSpecie.executeUpdate();

                if (id > maxSpecieId) {
                    maxSpecieId = id;
                }
            }
            statementInsertSpecieSequence.setLong(1, maxSpecieId + 1);
            statementInsertSpecieSequence.executeUpdate();
        } catch (Exception e) {
            System.err.println("V2__PopulateFromSWAPI.insertSpecies() error");
            throw e;
        }
    }

    private void insertPeople(API api, Context context) throws Exception {
        try {
            List<Person> people = api.getPeople();
            long maxPersonId = 0;
            PreparedStatement statementInsertPerson = context.getConnection()
                    .prepareStatement("INSERT INTO person (id, name, mass, birth_year) VALUES (?, ?, ?, ?);");
            PreparedStatement statementInsertPersonSequence = context.getConnection()
                    .prepareStatement("INSERT INTO person_sequence (next_val) VALUES (?);");
            PreparedStatement statementInsertPersonSpecies = context.getConnection()
                    .prepareStatement("INSERT INTO person_species (person_id, specie_id) VALUES (?, ?);");
            PreparedStatement statementInsertPersonFilms = context.getConnection()
                    .prepareStatement("INSERT INTO person_films (person_id, film_id) VALUES (?, ?);");

            for (Person person : people) {
                long id = api.getIdFromUrl(person.getUrl()).longValue();
                statementInsertPerson.setLong(1, id);
                statementInsertPerson.setString(2, person.getName());
                statementInsertPerson.setString(3, person.getMass());
                statementInsertPerson.setString(4, person.getBirth_year());
                statementInsertPerson.executeUpdate();

                for (String url : person.getSpecies()) {
                    long specieId = api.getIdFromUrl(url).longValue();

                    statementInsertPersonSpecies.setLong(1, id);
                    statementInsertPersonSpecies.setLong(2, specieId);
                    statementInsertPersonSpecies.executeUpdate();
                }
                for (String url : person.getFilms()) {
                    long filmId = api.getIdFromUrl(url).longValue();

                    statementInsertPersonFilms.setLong(1, id);
                    statementInsertPersonFilms.setLong(2, filmId);
                    statementInsertPersonFilms.executeUpdate();
                }
                if (id > maxPersonId) {
                    maxPersonId = id;
                }
            }
            statementInsertPersonSequence.setLong(1, maxPersonId + 1);
            statementInsertPersonSequence.executeUpdate();
        } catch (Exception e) {
            System.err.println("V2__PopulateFromSWAPI.insertPeople() error");
            throw e;
        }
    }
}
