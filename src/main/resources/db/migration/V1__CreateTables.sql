CREATE TABLE `person` (
	`id` BIGINT(20) NOT NULL,
	`birth_year` VARCHAR(255) NULL DEFAULT NULL,
	`mass` VARCHAR(255) NULL DEFAULT NULL,
	`name` VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=MyISAM
;
CREATE TABLE `person_sequence` (
	`next_val` BIGINT(20) NULL DEFAULT NULL
)
COLLATE='latin1_swedish_ci'
ENGINE=MyISAM
;
CREATE TABLE `film` (
	`id` BIGINT(20) NOT NULL,
	`title` VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=MyISAM
;
CREATE TABLE `film_sequence` (
	`next_val` BIGINT(20) NULL DEFAULT NULL
)
COLLATE='latin1_swedish_ci'
ENGINE=MyISAM
;
CREATE TABLE `specie` (
	`id` BIGINT(20) NOT NULL,
	`name` VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=MyISAM
;
CREATE TABLE `specie_sequence` (
	`next_val` BIGINT(20) NULL DEFAULT NULL
)
COLLATE='latin1_swedish_ci'
ENGINE=MyISAM
;
CREATE TABLE `person_films` (
	`person_id` BIGINT(20) NOT NULL,
	`film_id` BIGINT(20) NOT NULL,
	PRIMARY KEY (`person_id`, `film_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=MyISAM
;
CREATE TABLE `person_species` (
	`person_id` BIGINT(20) NOT NULL,
	`specie_id` BIGINT(20) NOT NULL,
	PRIMARY KEY (`person_id`, `specie_id`),
	INDEX `FK4x1kg2iab86vdfhqcqkqm16eq` (`specie_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=MyISAM
;
